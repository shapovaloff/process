<?php
require ('vendor/autoload.php');
use App\Process;
$payload = file_get_contents('php://input');
$process = new Process($_SERVER['REMOTE_ADDR'], $payload);
$result = $process->getPayload();
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
print json_encode($result);