<?php
namespace App;

use Predis\Client;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

/**
 * Class Process
 * @package App
 */
class Process
{
    /**
     * @var string
     */
    const TOKEN = '535083511:AAGDgnisM4yVGhiz-8laWjwWTrUN2KqfsvQ';
    const CHAT_ID = '-1001461067645';
    const REDIS = 'tcp://localhost:6379';
    private $ip;
    private $payload;

    /**
     * @var Logger
     */
    private $loggerPayload;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * Process constructor.
     * @param $ip
     * @param $payload
     */
    public function __construct(
        $ip,
        $payload
    ) {
        $this->payload = $payload;
        $this->ip = $ip;
        $this->loggerPayload = new Logger('loggerPayload');
        $this->loggerPayload->pushHandler(new StreamHandler(__DIR__.'/../../var/log/payload.log', Logger::INFO));
        $this->logger = new Logger('logger');
        $this->logger->pushHandler(new StreamHandler(__DIR__.'/../../var/log/logger.log', Logger::WARNING));
    }


    /**
     * @var string
     */
    const COUNTRY_LIST = [
        'Benin',
        'Cameroon',
        'Congo',
        'Senegal',
        'South Africa',
        'Burkina Faso',
        'Ivory Coast',
        'DR Congo',
        'Guinea',
        'Congo Republic',
        'Togo',
        'Gabon',
        'Morocco',
        'Nigeria',
        'Mali',
        'Niger',
        'France',
        'Italy',
        'Tunisia'
    ];

    /**
     * @return array
     */
    public function getPayload()
    {
        $this->loggerPayload->info($this->payload);
        $result = [
            'status'=> $this->getStatus()?'true':'false',
            'partners'=>[
                [
                    'pattern' => 'partners\/app\/android\/1xbet',
                    'link' => 'https://testssss.xyz/reg.php'
                ]
            ]
        ];

        return $result;
    }

    /**
     * @return bool
     */
    public function getStatus()
    {
        $result = false;
        try {
            $redis = new Client(self::REDIS);
            $country = $redis->get($this->ip);
            if (!$country) {
                $country = $this->getCountryByIp();
                $redis->set($this->ip, $country);
            }
            $result = in_array($country, self::COUNTRY_LIST);
            $this->sendMessage($country,$result);
        } catch (\Exception $ex) {
            $this->logger->warning($ex->getMessage());
        }

        return $result;
    }

    /**
     * @return string
     */
    private function getCountryByIp()
    {
        $country = '';
        try {
            $res = file_get_contents('https://www.iplocate.io/api/lookup/' . $this->ip . '?apikey=233910e67a355b101203977d8ab9f875');
            $res = json_decode($res);
            $country = $res->country;
        } catch (\Exception $ex) {
            $this->logger->warning($ex->getMessage());
        }

        return $country;
    }

    /**
     * @param string $country
     * @param bool $status
     * @return bool|string
     */
    public function sendMessage($country, $status)
    {
        $result =false;
        if ($this->payload) {
            $isOk = $status ? hex2bin('E29C85') : hex2bin('E29D8C');
            $message = '<b>' . $country . '</b> ' . $isOk . ' ' . $this->payload;
            $url = "https://api.telegram.org/bot" . self::TOKEN . "/sendMessage?chat_id=" . self::CHAT_ID;
            $url = $url . "&text=" . urlencode($message) . "&parse_mode=html";
            $ch = curl_init();
            $optArray = array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true
            );
            curl_setopt_array($ch, $optArray);
            $result = curl_exec($ch);
            curl_close($ch);
        }

        return $result;
    }
}