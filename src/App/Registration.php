<?php
namespace App;

class Registration
{
    /**
     * @var string
     */
    const TOKEN = '535083511:AAGDgnisM4yVGhiz-8laWjwWTrUN2KqfsvQ';
    const CHAT_ID_REG = '-1001169994605';
    const CHAT_ID_WARNING = '-1001197197543';
    const URL = 'http://refpa164592.top/L?tag=s_536519m_1234c_ads_p&site=536519&ad=1234&r=fr/registration';

    /**
     * @return bool|string
     */
    public function sendMessageReg()
    {
        $message = hex2bin('E29C85')."Redirect reg ";
        $url = "https://api.telegram.org/bot" . self::TOKEN . "/sendMessage?chat_id=" . self::CHAT_ID_REG;
        $url = $url . "&text=" . urlencode($message) . "&parse_mode=html";
        $ch = curl_init();
        $optArray = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true
        );
        curl_setopt_array($ch, $optArray);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    /**
     * @return bool|string
     */
    public function sendMessageWarning()
    {
        $message = hex2bin('E29C85')."REDIRECT FROM ANOTHER SOURCE";
        $url = "https://api.telegram.org/bot" . self::TOKEN . "/sendMessage?chat_id=" . self::CHAT_ID_WARNING;
        $url = $url . "&text=" . urlencode($message) . "&parse_mode=html";
        $ch = curl_init();
        $optArray = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true
        );
        curl_setopt_array($ch, $optArray);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    /**
     * @param false $permanent
     */
    public function redirect($permanent = false)
    {
        header('Location: ' . self::URL, true, $permanent ? 301 : 302);
        exit();
    }

    /**
     * @param $source
     * @return bool
     */
    public function checkSource($source)
    {
        $status = false;
        if ($source && preg_match('/sportpari/', $source))
        {
            $status = true;
        }

        return $status;
    }

}