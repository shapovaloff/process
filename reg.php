<?php
require ('vendor/autoload.php');
use App\Registration;
$registration = new Registration();
if ($registration->checkSource($_SERVER['HTTP_REFERER'])) {
    $registration->sendMessageReg();
    $registration->redirect();
} else {
    $registration->sendMessageWarning();
    header("HTTP/1.0 404 Not Found");
    die;
}